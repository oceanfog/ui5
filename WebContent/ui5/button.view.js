sap.ui.jsview("ui5.button", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf ui5.button
	*/ 
	getControllerName : function() {
		return "ui5.button";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf ui5.button
	*/ 
	createContent : function(oController) {
		oButton = new sap.ui.commons.Button({
			id : "", // sap.ui.core.ID
			text : '', // string
			enabled : true, // boolean
			visible : true, // boolean
			width : undefined, // sap.ui.core.CSSSize
			helpId : '', // string
			icon : '', // sap.ui.core.URI
			iconHovered : '', // sap.ui.core.URI
			iconSelected : '', // sap.ui.core.URI
			iconFirst : true, // boolean
			height : undefined, // sap.ui.core.CSSSize
			styled : true, // boolean
			lite : false, // boolean
			style : sap.ui.commons.ButtonStyle.Default, // sap.ui.commons.ButtonStyle
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "", // sap.ui.core.ID
				key : undefined, // string
				value : undefined, // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.CustomData
			ariaDescribedBy : [], // sap.ui.core.Control
			ariaLabelledBy : [], // sap.ui.core.Control
			press : [ function(oEvent) {
				var control = oEvent.getSource();
				oController.printHello;
			}, this ]
		});
		
		return oButton;
	}

});
