sap.ui.jsview("ui5.fb70", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf ui5.fb70
	*/ 
	getControllerName : function() {
		return "ui5.fb70";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf ui5.fb70
	*/ 
	createContent : function(oController) {
		oGrid = new sap.ui.layout.Grid({
			id : "id", // sap.ui.core.ID
			width : "100%", // sap.ui.core.CSSSize
			vSpacing : 1, // int
			hSpacing : 1, // int
			position : sap.ui.layout.GridPosition.Left, // sap.ui.layout.GridPosition
			defaultSpan : "L3 M6 S12", // sap.ui.layout.GridSpan
			defaultIndent : "L0 M0 S0", // sap.ui.layout.GridIndent
			containerQuery : false, // boolean
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "id1", // sap.ui.core.ID
				key : undefined, // string
				value : undefined, // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.CustomData
			content : [
				new sap.ui.commons.TextView({
				    text : 'Label spanned over 4 cols on large, 6 cols on medium, and 12 cols on small screens.',
				    layoutData : new sap.ui.layout.GridData({
				            span : "L4 M6 S12"
				    })
				}),
			           ]
		// sap.ui.core.Control
		});
		
		return oGrid;
	
		
		label
	
	}

});
