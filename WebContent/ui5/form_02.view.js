sap.ui.jsview("ui5.form_02", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf ui5.form_02
	*/ 
	getControllerName : function() {
		return "ui5.form_02";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf ui5.form_02
	*/ 
	createContent : function(oController) {
		oForm_02 = new sap.ui.commons.form.SimpleForm({
			id : "form_02", // sap.ui.core.ID
			maxContainerCols : 2, // int
			minWidth : -1, // int
			editable : undefined, // boolean
			labelMinWidth : 192, // int
			layout : sap.ui.layout.form.SimpleFormLayout.ResponsiveLayout, // sap.ui.layout.form.SimpleFormLayout
			labelSpanL : 4, // int, since 1.16.3
			labelSpanM : 2, // int, since 1.16.3
			labelSpanS : 12, // int, since 1.16.3
			emptySpanL : 0, // int, since 1.16.3
			emptySpanM : 0, // int, since 1.16.3
			emptySpanS : 0, // int, since 1.16.3
			columnsL : 2, // int, since 1.16.3
			columnsM : 1, // int, since 1.16.3
			breakpointL : 1024, // int, since 1.16.3
			breakpointM : 600, // int, since 1.16.3
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "", // sap.ui.core.ID
				key : undefined, // string
				value : "customData value", // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.CustomData
			content : [], // sap.ui.core.Element
			title : new sap.ui.core.Title({
				id : "", // sap.ui.core.ID
				text : "hello", // string
				icon : undefined, // sap.ui.core.URI
				level : sap.ui.core.TitleLevel.Auto, // sap.ui.core.TitleLevel
				emphasized : false, // boolean
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			})
		// sap.ui.core.Title, since 1.16.3
		});
		
		return oForm_02;
	}

});
