sap.ui.jsview("ui5.form_03", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf ui5.form_03
	*/ 
	getControllerName : function() {
		return "ui5.form_03";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf ui5.form_03
	*/ 
	createContent : function(oController) {
		
		
		
		oTextField_01 =  new sap.ui.commons.TextField({value: "Max"});
        oTextField_02 = new sap.ui.commons.TextField({value: "Mustermann"});
        oTextField_03 = oTextField_01;   
        
        
        oLabel_01 = new sap.ui.commons.Label({
			id : "", // sap.ui.core.ID
			design : sap.ui.commons.LabelDesign.Standard, // sap.ui.commons.LabelDesign
			textDirection : sap.ui.core.TextDirection.Inherit, // sap.ui.core.TextDirection
			wrapping : false, // boolean
			width : '', // sap.ui.core.CSSSize
			text : "lable_01", // string
			visible : true, // boolean, since 1.14.0
			icon : undefined, // sap.ui.core.URI
			textAlign : sap.ui.core.TextAlign.Begin, // sap.ui.core.TextAlign
			required : false, // boolean, since 1.11.0
			requiredAtBegin : undefined, // boolean, since 1.14.0
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "", // sap.ui.core.ID
				key : undefined, // string
				value : undefined, // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.CustomData
			labelFor : undefined
		// sap.ui.core.Control
		});
        
        oLabel_02 = new sap.ui.commons.Label({text : "lable_02", });
        oLabel_03 = new sap.ui.commons.Label({text : "lable_03", });
        
        
        oFormElement_01 = new sap.ui.layout.form.FormElement({
			id : "", // sap.ui.core.ID
			visible : true, // boolean
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "", // sap.ui.core.ID
				key : undefined, // string
				value : undefined, // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.CustomData
			label : undefined, // sap.ui.core.Label
			fields : [oLabel_01, oTextField_01]
		// sap.ui.core.Control
		});
        
        oFormElement_02 = new sap.ui.layout.form.FormElement({
			label : "", // sap.ui.core.Label
			fields : [
			          oLabel_02,
			          ]
		// sap.ui.core.Control
		});
        
        oFormElement_03 = new sap.ui.layout.form.FormElement({
			label : "", // sap.ui.core.Label
			fields : [
			          oLabel_03,
			          ]
		// sap.ui.core.Control
		});
        

        
        
        oFormContainer_01 = new sap.ui.layout.form.FormContainer({
			formElements : [
			                oFormElement_01
			                ], // sap.ui.layout.form.FormElement
			title : new sap.ui.core.Title({
				text : "기본사항", // string
			// sap.ui.core.CustomData
			}),
		// sap.ui.core.Title
		});
        
        oFormContainer_02 = new sap.ui.layout.form.FormContainer({
			formElements : [ 
			                 oFormElement_02 
			               ], // sap.ui.layout.form.FormElement
			title : new sap.ui.core.Title({
				text : "세부사항", // string
			// sap.ui.core.CustomData
			}),
			
		// sap.ui.core.Title
		});
        
        oFormContainer_03 = new sap.ui.layout.form.FormContainer({
			formElements : [ 
			                 oFormElement_02 
			               ], // sap.ui.layout.form.FormElement
			title : new sap.ui.core.Title({
				text : "세부사항", // string
			// sap.ui.core.CustomData
			}),
			
		// sap.ui.core.Title
		});
        
		oForm_03 = new sap.ui.layout.form.Form({
			id : "", // sap.ui.core.ID
			width : undefined, // sap.ui.core.CSSSize
			visible : true, // boolean
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "", // sap.ui.core.ID
				key : undefined, // string
				value : undefined, // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.CustomData
			formContainers : [
			                  oFormContainer_01,
			                  oFormContainer_02,
			                  oFormContainer_03,
			                  ], // sap.ui.layout.form.FormContainer
			title : undefined, // sap.ui.core.Title
			layout : new sap.ui.layout.form.FormLayout({
				id : "", // sap.ui.core.ID
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			})
		// sap.ui.layout.form.FormLayout
		})
		
		return oForm_03;
	}

});
