sap.ui.jsview("ui5.grid_01", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf ui5.grid_01
	*/ 
	getControllerName : function() {
		return "ui5.grid_01";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf ui5.grid_01
	*/ 
	createContent : function(oController) {
		oLabel_gv01_01 = new sap.ui.commons.Label({text : "요청번호"});
		oLabel_gv01_02 = new sap.ui.commons.Label({text : "전표번호"});
		oLabel_gv01_03 = new sap.ui.commons.Label({text : "전기일자"});
		oLabel_gv01_04 = new sap.ui.commons.Label({text : "증빙일자"});
		oLabel_gv01_05 = new sap.ui.commons.Label({text : "통화"});
		oLabel_gv01_06 = new sap.ui.commons.Label({text : "적요"});
		oLabel_gv01_07 = new sap.ui.commons.Label({text : "lable_02"});
		
		
		oTextField_04 =  new sap.ui.commons.TextField({value: "Max"});
		oTextField_05 =  new sap.ui.commons.TextField({value: "Max"});
		oTextField_06 =  new sap.ui.commons.TextField({value: "Max"});
		oTextField_07 =  new sap.ui.commons.TextField({value: "Max"});
		
		oTextField_gv01_08 =  new sap.ui.commons.TextField({value: "Max"});
		oTextField_gv01_09 =  new sap.ui.commons.TextField({value: "Max"});
		
		oGrid_01 = new sap.ui.layout.Grid({
			id : "", // sap.ui.core.ID
			width : "100%", // sap.ui.core.CSSSize
			vSpacing : 1, // int
			hSpacing : 1, // int
			defaultSpan : "L3 M6 S12", // sap.ui.layout.GridSpan
			defaultIndent : "L0 M0 S0", // sap.ui.layout.GridIndent
			content : [
			           oTextField_04, oTextField_05
			           ]
		// sap.ui.core.Control
		});
		

		
		oGrid_gv01_01 = new sap.ui.layout.Grid({
			id : "", // sap.ui.core.ID
			width : "100%", // sap.ui.core.CSSSize
			vSpacing : 1, // int
			hSpacing : 1, // int
			position : sap.ui.layout.GridPosition.Left, // sap.ui.layout.GridPosition
			defaultSpan : "L3 M6 S12", // sap.ui.layout.GridSpan
			defaultIndent : "L0 M0 S0", // sap.ui.layout.GridIndent
			content : [
			           	oLabel_gv01_01,
			           	oTextField_gv01_08,
			           	oLabel_gv01_02,
			           	oTextField_gv01_09
			           ]
		// sap.ui.core.Control
		});
		
		oGrid_gv01_02 = new sap.ui.layout.Grid({
			id : "", // sap.ui.core.ID
			width : "100%", // sap.ui.core.CSSSize
			vSpacing : 1, // int
			hSpacing : 1, // int
			position : sap.ui.layout.GridPosition.Left, // sap.ui.layout.GridPosition
			defaultSpan : "L3 M6 S12", // sap.ui.layout.GridSpan
			defaultIndent : "L0 M0 S0", // sap.ui.layout.GridIndent
			content : [
			           	oLabel_gv01_03,
			           	oTextField_gv01_08,
			           	oLabel_gv01_04,
			           	oTextField_gv01_09
			           ]
		// sap.ui.core.Control
		});
		
		oGrid_gv01_03 = new sap.ui.layout.Grid({
			id : "", // sap.ui.core.ID
			width : "100%", // sap.ui.core.CSSSize
			vSpacing : 1, // int
			hSpacing : 1, // int
			position : sap.ui.layout.GridPosition.Left, // sap.ui.layout.GridPosition
			defaultSpan : "L3 M6 S12", // sap.ui.layout.GridSpan
			defaultIndent : "L0 M0 S0", // sap.ui.layout.GridIndent
			content : [
			           	oLabel_gv01_05,
			           ]
		// sap.ui.core.Control
		});
		
		oGrid_gv01_04 = new sap.ui.layout.Grid({
			id : "", // sap.ui.core.ID
			width : "100%", // sap.ui.core.CSSSize
			vSpacing : 1, // int
			hSpacing : 1, // int
			position : sap.ui.layout.GridPosition.Left, // sap.ui.layout.GridPosition
			defaultSpan : "L3 M6 S12", // sap.ui.layout.GridSpan
			defaultIndent : "L0 M0 S0", // sap.ui.layout.GridIndent
			content : [
			           	oLabel_gv01_06,
			           ]
		// sap.ui.core.Control
		});
		
		
		oFormElemnt_gv01_01 = new sap.ui.layout.form.FormElement({
			id : "", // sap.ui.core.ID
			visible : true, // boolean
			tooltip : undefined, // sap.ui.core.TooltipBase
			label : "기본 사항", // sap.ui.core.Label
			fields : [
			          oGrid_gv01_01,
			          oGrid_gv01_02,
			          oGrid_gv01_03,
			          oGrid_gv01_04,
			          
			          ]
		// sap.ui.core.Control
		});
		oFormElemnt_gv01_02 = new sap.ui.layout.form.FormElement({
			id : "", // sap.ui.core.ID
			visible : true, // boolean
			tooltip : undefined, // sap.ui.core.TooltipBase
			label : "세부 사항", // sap.ui.core.Label
			fields : [
			          
			          ]
		// sap.ui.core.Control
		});
		
		oForm_fb70 = new sap.ui.layout.form.Form({
			id : "", // sap.ui.core.ID
			width : undefined, // sap.ui.core.CSSSize
			visible : true, // boolean
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "", // sap.ui.core.ID
				key : undefined, // string
				value : undefined, // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.CustomData
			formContainers : [ new sap.ui.layout.form.FormContainer({
				id : "", // sap.ui.core.ID
				expanded : true, // boolean
				expandable : false, // boolean
				visible : true, // boolean
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : [], // sap.ui.core.CustomData
				formElements : [ 
				                oFormElemnt_gv01_01,
				                oFormElemnt_gv01_02,
				                
				                ], // sap.ui.layout.form.FormElement
				title : new sap.ui.core.Title({
					id : "", // sap.ui.core.ID
					text : "FormContainer", // string
					icon : undefined, // sap.ui.core.URI
					level : sap.ui.core.TitleLevel.Auto, // sap.ui.core.TitleLevel
					emphasized : false, // boolean
					tooltip : undefined, // sap.ui.core.TooltipBase
					customData : []
				// sap.ui.core.CustomData
				})
			// sap.ui.core.Title
			}) ], // sap.ui.layout.form.FormContainer
			title : undefined, // sap.ui.core.Title
			layout : new sap.ui.layout.form.FormLayout({
				id : "", // sap.ui.core.ID
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			})
		// sap.ui.layout.form.FormLayout
		});
		
		
		return oForm_fb70;
	}

});

