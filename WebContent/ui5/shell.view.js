sap.ui.jsview("ui5.shell", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf ui5.shell
	*/ 
	getControllerName : function() {
		return "ui5.shell";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf ui5.shell
	*/ 
	createContent : function(oController) {
		oShell = new sap.ui.ux3.Shell({
			id : "shell_01", // sap.ui.core.ID
			appTitle : undefined, // string
			appIcon : undefined, // sap.ui.core.URI
			appIconTooltip : undefined, // string
			showLogoutButton : true, // boolean
			logoutButtonTooltip : undefined, // string, since 1.9.0
			showSearchTool : true, // boolean
			showInspectorTool : false, // boolean
			showFeederTool : true, // boolean
			showTools : true, // boolean
			showPane : true, // boolean
			headerType : sap.ui.ux3.ShellHeaderType.SlimNavigation, // sap.ui.ux3.ShellHeaderType
			designType : sap.ui.ux3.ShellDesignType.Crystal, // sap.ui.ux3.ShellDesignType, since 1.12.0
			paneWidth : 250, // int
			applyContentPadding : true, // boolean, since 1.9.0
			fullHeightContent : false, // boolean, since 1.9.0
			allowOverlayHeaderAccess : false, // boolean, since 1.14.0
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "cd_01", // sap.ui.core.ID
				key : undefined, // string
				value : undefined, // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.CustomData
			worksetItems : [ new sap.ui.ux3.NavigationItem({
				id : "wsi_01", // sap.ui.core.ID
				text : "form_01", // string
				enabled : true, // boolean
				textDirection : sap.ui.core.TextDirection.Inherit, // sap.ui.core.TextDirection
				key : undefined, // string
				visible : true, // boolean, since 1.9.0
				href : undefined, // sap.ui.core.URI
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : [], // sap.ui.core.CustomData
				subItems : []
			// sap.ui.ux3.NavigationItem
			}),
			new sap.ui.ux3.NavigationItem({id:"wsi_02", key:"wsi_02",text:"Business Partner"}),
			new sap.ui.ux3.NavigationItem({id:"wsi_03", key:"wsi_03",text:"form_03"}),
			new sap.ui.ux3.NavigationItem({id:"wsi_04", key:"wsi_04",text:"grid_01"}),
			new sap.ui.ux3.NavigationItem({id:"wsi_05", key:"wsi_05",text:"tree"}),
			new sap.ui.ux3.NavigationItem({id:"wsi_06", key:"wsi_06",text:"splitter"}),
			
			
			], // sap.ui.ux3.NavigationItem
			
			paneBarItems : [ new sap.ui.core.Item({
				id : "pbi_01", // sap.ui.core.ID
				text : "", // string
				enabled : true, // boolean
				textDirection : sap.ui.core.TextDirection.Inherit, // sap.ui.core.TextDirection
				key : undefined, // string
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.Item
			paneContent : [], // sap.ui.core.Control
			content : [], // sap.ui.core.Control
//			toolPopups : [ new sap.ui.ux3.ToolPopup({
//				id : "tp_01", // sap.ui.core.ID
//				title : undefined, // string
//				icon : undefined, // sap.ui.core.URI
//				iconHover : undefined, // sap.ui.core.URI
//				iconSelected : undefined, // sap.ui.core.URI
//				modal : false, // boolean
//				inverted : true, // boolean, since 1.11.1
//				autoClose : false, // boolean, since 1.13.2
//				maxHeight : undefined, // sap.ui.core.CSSSize, since 1.13.2
//				maxWidth : undefined, // sap.ui.core.CSSSize, since 1.15.0
//				tooltip : undefined, // sap.ui.core.TooltipBase
//				customData : [], // sap.ui.core.CustomData
//				buttons : [], // sap.ui.core.Control
//				content : [], // sap.ui.core.Control
//				initialFocus : undefined, // sap.ui.core.Control
//				opener : undefined, // sap.ui.core.Control
//				open : [ function(oEvent) {
//					var control = oEvent.getSource();
//				}, this ],
//				close : [ function(oEvent) {
//					var control = oEvent.getSource();
//				}, this ],
//				enter : [ function(oEvent) {
//					var control = oEvent.getSource();
//				}, this ],
//				iconChanged : [ function(oEvent) {
//					var control = oEvent.getSource();
//				}, this ],
//				closed : [ function(oEvent) {
//					var control = oEvent.getSource();
//				}, this ]
//			}) ], // sap.ui.ux3.ToolPopup
			headerItems : [], // sap.ui.core.Control
			notificationBar : new sap.ui.ux3.NotificationBar({
				id : "id5", // sap.ui.core.ID
				visibleStatus : sap.ui.ux3.NotificationBarStatus.Default, // sap.ui.ux3.NotificationBarStatus
				resizeEnabled : true, // boolean
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : [], // sap.ui.core.CustomData
				messageNotifier : undefined, // sap.ui.core.Element
				notifiers : [], // sap.ui.core.Element
				display : [ function(oEvent) {
					var control = oEvent.getSource();
				}, this ],
				resize : [ function(oEvent) {
					var control = oEvent.getSource();
				}, this ]
			// since 1.12.2
			}), // sap.ui.ux3.NotificationBar, since 1.7.0
			selectedWorksetItem : undefined, // sap.ui.ux3.NavigationItem
			worksetItemSelected : [ function(oEvent) {
				var control = oEvent.getSource();
				var sId = oEvent.getParameter("id");
                var oShell = oEvent.oSource;
                switch (sId) {
                case "wsi_01":
	                oShell.setContent(view_ex_form_01);
	                break;
                case "wsi_02":
	                oShell.setContent(view_ex_fb70);
	                break;
                case "wsi_03":
	                oShell.setContent(view_form_03);
	                break;
                case "wsi_04":
	                oShell.setContent(view_grid_01);
	                break;
                case "wsi_05":
	                oShell.setContent(view_tree);
	                break;
                case "wsi_06":
	                oShell.setContent(view_splitter);
	                break;
                case "wsi_07":
	                oShell.setContent();
	                break;
                }
				
			}, this ],
			paneBarItemSelected : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ],
			logout : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ],
			search : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ],
			feedSubmit : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ],
			paneClosed : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ]
		// since 1.12.0
		});
		
		return oShell;
	}

});
